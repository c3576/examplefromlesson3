#include "Student.h"
#include <iostream>;

using std::cout;
using std::endl;



void dontCopy(Student s)
{
	// creates a student with default values
	Student newStudent(-1, "", "");
	newStudent.setId(s.getId());
	newStudent.setFirstName(s.getFirstName());
	newStudent.setLastName(s.getLastName());
	// grades are not copied
}
int main()
{
	Student stud(123456789, "Blinky", "Bill");
	Student stud1(stud);
	dontCopy(stud);
	system("pause");
	return 0;
}